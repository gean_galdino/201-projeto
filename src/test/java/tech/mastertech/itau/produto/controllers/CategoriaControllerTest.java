package tech.mastertech.itau.produto.controllers;

import static org.mockito.ArgumentMatchers.any;
//import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.services.CategoriaService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CategoriaController.class)
public class CategoriaControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private CategoriaService categoriaService;
  
  private ObjectMapper mapper = new ObjectMapper();
  private Categoria categoria;
  
  @Before
  public void preparar() {
    categoria = new Categoria();
    categoria.setNome("Álcool");
  }
  
  @Test
  public void deveBuscarTodasAsCategorias() throws Exception {
    List<Categoria> categorias = Lists.list(categoria);
    
    when(categoriaService.getCategorias()).thenReturn(categorias);
    
    mockMvc.perform(get("/categorias"))
            .andExpect(status().isOk())
            .andExpect(content().string(mapper.writeValueAsString(categorias)));
  }
  
  @Test
  @WithMockUser
  public void deveSalvarUmaCategoria() throws Exception {
    when(categoriaService.setCategoria(any(Categoria.class))).thenReturn(categoria);
    
    String categoriaJson = mapper.writeValueAsString(categoria);
    
    mockMvc.perform(
        post("/auth/categoria")
        .content(categoriaJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
       )
      .andExpect(status().isOk())
      .andExpect(content().string(categoriaJson));
  }
}
