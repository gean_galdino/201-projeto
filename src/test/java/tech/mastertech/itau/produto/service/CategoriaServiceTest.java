package tech.mastertech.itau.produto.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.repositories.CategoriaRepository;
import tech.mastertech.itau.produto.services.CategoriaService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CategoriaService.class)
public class CategoriaServiceTest {
	
	@Autowired
	private CategoriaService sujeito;
	
	@MockBean
	private CategoriaRepository categoriaRepository;
	
	@Test
	public void deveSalvarUmaCategoria() {
		Categoria categoria = new Categoria();
		//categoria.setId(1);
		categoria.setNome("Alcool");
		
		Mockito.when(categoriaRepository.save(categoria)).thenReturn(categoria);
		
		Categoria categoriaSalva = sujeito.setCategoria(categoria);
		assertEquals(categoriaSalva.getNome(), categoria.getNome());
	}
	
	@Test
	public void deveRetornarListaDeCategoria() {
		Categoria categoria = new Categoria();
		categoria.setId(1);
		categoria.setNome("Alcool");
		List<Categoria> categorias = Lists.list(categoria);
		Mockito.when(categoriaRepository.findAll()).thenReturn(categorias);
		
		
		Iterable<Categoria> categoriasEncontradasInterable = sujeito.getCategorias();
		List<Categoria> categoriasEncontradas = Lists.newArrayList(categoriasEncontradasInterable);
		
		assertEquals(1, categoriasEncontradas.size());
		assertEquals(categoria, categoriasEncontradas.get(0));
	}
}
