package tech.mastertech.itau.produto.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.repositories.FornecedorRepository;
import tech.mastertech.itau.produto.services.FornecedorService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = FornecedorService.class)

public class FornecedorServiceTest {
	@Autowired
	private FornecedorService fornecedorService;
	
	@MockBean
	private FornecedorRepository fornecedorRepository;
	
	@Test
	public void vaiGravarUmFornecedor() {
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setNome("Diadema Delivery");
		
	}
}
