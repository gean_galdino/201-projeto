package tech.mastertech.itau.produto.services;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.repositories.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ProdutoService.class)
public class ProdutoServiceTest {
  @Autowired
  private ProdutoService sujeito;
  
  @MockBean
  private ProdutoRepository produtoRepository;
  
  @Test
  public void deveDeletarUmProduto() {
    int idProduto = 1;
    
    sujeito.deletar(idProduto);
    
    verify(produtoRepository).deleteById(idProduto);
  }
}
