package tech.mastertech.itau.produto.controllers;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.models.Produto;
import tech.mastertech.itau.produto.services.CategoriaService;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
public class ProdutoController {
  
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping("/produtos")
	public Iterable<Produto> getProdutos(@RequestParam(required=false) String nome){
		if(nome == null) {
			return produtoService.getProdutos();	
		}
		Optional<Categoria> categoriaOptional = categoriaService.getCategoriaByNome(nome);
		
		if(!categoriaOptional.isPresent()) {
		  return new ArrayList<Produto>();
		}
		
		return produtoService.getProdutos(categoriaOptional.get());
	}
	
	@GetMapping("/produto/{idProduto}")
	public Produto getProduto(@PathVariable int idProduto) {
		return produtoService.getProduto(idProduto);
	}
	
	@PostMapping("/auth/produto")
	public Produto setProduto(@RequestBody Produto produto) {
		return produtoService.setProduto(produto);
	}
	
	@PatchMapping("/auth/produto/{idProduto}/valor")
	public Produto mudarValor(@PathVariable int idProduto, @RequestBody Map<String, Double> valor) {
	    Double numero = valor.get("valor");
	  
	    if(numero == null) {
	      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Deve informar um valor");
	    }
	    
		return produtoService.mudarValor(idProduto, valor.get("valor"));
	}
	
}
